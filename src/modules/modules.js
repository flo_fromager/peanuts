// HTML PARAMETERS

const getParam = (parameterName) => {
  var result = null,
    tmp = [];
  var items = location.search.substr(1).split("&");
  for (var index = 0; index < items.length; index++) {
    tmp = items[index].split("=");
    if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
  }
  return result;
};

const allSet = (queryParams, set, value) => {
  queryParams.set(set, value);
  localStorage.setItem(set, value);
};

// SIMPLE FUNCTION

const rewriteCaps = (name, factor) => {
  let names = name.split(factor);
  if (names.length > 0) {
    name = "";
    for (let i = 0; i < names.length; i++) {
      let part = names[i];
      part = part.charAt(0).toUpperCase() + part.slice(1);
      name += i > 0 ? `${factor}${part}` : part;
    }
  }
  return name;
};

const rewriteName = (name) => {
  name = name.toLowerCase();
  name = rewriteCaps(name, " ");
  name = rewriteCaps(name, "-");

  return name;
};

const alertMsg = (alert, data) => {
  alert.value = data;
  setTimeout(() => {
    alert.value = undefined;
  }, 2000);
};

const id = () => {
  var date = Date.now().toString();
  return date.substring(8, date.length - 2);
};

export { getParam, rewriteName, alertMsg, allSet, id };
